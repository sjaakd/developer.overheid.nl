// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
module.exports = {
  extends: '@commonground/eslint-config-cra-standard-prettier',
  rules: {
    '@typescript-eslint/no-empty-function': [
      'error',
      {
        allow: ['arrowFunctions']
      },
    ],
    'react/display-name': 'off',
    'security/detect-object-injection': 'off',
  },
}
