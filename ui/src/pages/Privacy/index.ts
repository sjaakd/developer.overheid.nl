// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import Privacy from './privacy'

export { Privacy }
