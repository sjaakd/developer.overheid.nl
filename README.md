# developer.overheid.nl

This is the repository of the software backing the [developer.overheid.nl](https://developer.overheid.nl) portal. This portal provides an overview of all API's within the Dutch government.


## Adding an API to developer.overheid.nl

See [Submit your API](https://developer.overheid.nl/add-api).


## Developer documentation

If you would like to contribute to the developer.overheid.nl software itself, consult the [`CONTRIBUTING.md`](CONTRIBUTING.md) file.


## Licence

Copyright © VNG Realisatie 2019

[Licensed under the EUPLv1.2](LICENCE.md)
